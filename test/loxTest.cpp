#include "gtest/gtest.h"

#include "lox.h"

TEST(EmptyFileTest, emptyFile) {
    cpplox::ErrorReporter error_reporter;
    cpplox::Lox l(error_reporter);
    EXPECT_EQ(0, l.runFile("../test/empty_file.lox"));
}

TEST(NonExistentFileTest, nonExistentFile) {
    cpplox::ErrorReporter error_reporter;
    cpplox::Lox l(error_reporter);
    EXPECT_EQ(2, l.runFile("./nonExistentFile.lox"));
}
