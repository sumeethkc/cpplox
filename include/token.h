#ifndef TOKEN_H
#define TOKEN_H

#include <any>
#include <string>

#include "token_type.h"

namespace cpplox {
class Token {
 private:
  TokenType type;
  std::string lexeme;
  std::any literal;
  int line;

 public:
  Token(TokenType type, const std::string &lexeme, std::any literal, int line);
  std::string toString();
  std::string getTokenType();
};
}  // namespace cpplox
#endif
