#include "scanner.h"

#include <iostream>

namespace cpplox {

std::list<Token> Scanner::scanTokens() {
  while (!isAtEnd()) {
    // We are at the beginning of the next lexeme
    start = current;
    scanToken();
  }

  tokens.push_back(
      Token(TokenType::Value::LOX_EOF, "", std::any(nullptr), line));
  return tokens;
}

bool Scanner::isAtEnd() { return current >= source.length(); }

void Scanner::scanToken() {
  char c = advance();
  switch (c) {
    case '(':
      addToken(TokenType::Value::LEFT_PAREN);
      break;
    case ')':
      addToken(TokenType::Value::RIGHT_PAREN);
      break;
    case '{':
      addToken(TokenType::Value::LEFT_BRACE);
      break;
    case '}':
      addToken(TokenType::Value::RIGHT_BRACE);
      break;
    case ',':
      addToken(TokenType::Value::COMMA);
      break;
    case '.':
      addToken(TokenType::Value::DOT);
      break;
    case '-':
      addToken(TokenType::Value::MINUS);
      break;
    case '+':
      addToken(TokenType::Value::PLUS);
      break;
    case ';':
      addToken(TokenType::Value::SEMICOLON);
      break;
    case '*':
      addToken(TokenType::Value::STAR);
      break;
    case '!':
      addToken(match('=') ? TokenType::Value::BANG_EQUAL
                          : TokenType::Value::BANG);
      break;
    case '=':
      addToken(match('=') ? TokenType::Value::EQUAL_EQUAL
                          : TokenType::Value::EQUAL);
      break;
    case '<':
      addToken(match('=') ? TokenType::Value::LESS_EQUAL
                          : TokenType::Value::LESS);
      break;
    case '>':
      addToken(match('=') ? TokenType::Value::GREATER_EQUAL
                          : TokenType::Value::GREATER);
      break;
    case '/':
      if (match('/'))
        skipComment();
      else if (match('*'))
        skipCommentBlock();
      else
        addToken(TokenType::Value::SLASH);
      break;
    case ' ':
    case '\r':
    case '\t':
      // Ignore whitespace
      break;
    case '\n':
      ++line;
      break;
    case '"':
      createStringToken();
      break;
    default:
      if (isDigit(c)) {
        number();
      } else if (isAlpha(c)) {
        identifier();
      } else {
        std::string str("Unexpected character: ");
        str.append(1, static_cast<char>(c));
        error_reporter.report(line, str);
      }
      break;
  }
}

bool Scanner::isAlpha(char c) {
  return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
}

bool Scanner::isAlphaNumeric(char c) { return isAlpha(c) || isDigit(c); }

void Scanner::identifier() {
  while (isAlphaNumeric(peek())) advance();

  std::string text = source.substr(start, current - start);
  std::cout << "Identifier text: " << text << std::endl;
  addToken(getKeywordType(text));
}

TokenType::Value Scanner::getKeywordType(const std::string &text) {
  static const std::map<std::string, TokenType::Value> keywords{
      {"and", TokenType::Value::AND},       {"class", TokenType::Value::CLASS},
      {"else", TokenType::Value::ELSE},     {"false", TokenType::Value::FALSE},
      {"for", TokenType::Value::FOR},       {"fun", TokenType::Value::FUN},
      {"if", TokenType::Value::IF},         {"nil", TokenType::Value::NIL},
      {"or", TokenType::Value::OR},         {"print", TokenType::Value::PRINT},
      {"return", TokenType::Value::RETURN}, {"super", TokenType::Value::SUPER},
      {"this", TokenType::Value::THIS},     {"true", TokenType::Value::TRUE},
      {"var", TokenType::Value::VAR},       {"while", TokenType::Value::WHILE}};

  auto iter = keywords.find(text);
  if (iter == keywords.end()) {
    return TokenType::Value::IDENTIFIER;
  }
  return iter->second;
}

bool Scanner::isDigit(char c) { return c >= '0' && c <= '9'; }

void Scanner::number() {
  while (isDigit(peek())) advance();

  // Look for the decimal point
  if (peek() == '.' && isDigit(peekNext())) {
    // Consume the '.'
    advance();
    while (isDigit(peek())) advance();
  }

  std::string value = source.substr(start, current - start);
  addToken(TokenType::Value::NUMBER, std::any(std::stod(value)));
}

void Scanner::createStringToken() {
  while (peek() != '"' && !isAtEnd()) {
    if (peek() == '\n') {
      // Well, multi-line strings are supported
      ++line;
    }
    advance();
  }

  // Unterminated string
  if (isAtEnd()) {
    error_reporter.report(line, "Unterminated String.");
  } else {
    // Consume the closing '"'.
    advance();

    // substr starts from start + 1 because start is at beginning quote of
    // the string literal. current is at the character after the closing quote.
    // length = current - 2 - start
    std::string value =
        source.substr(start + 1, /*length*/ current - start - 2);
    addToken(TokenType::Value::STRING, std::any(value));
  }
}

void Scanner::skipComment() {
  while (peek() != '\n' && !isAtEnd()) {
    advance();
  }
}

void Scanner::skipCommentBlock() {
  std::cout << "In skip comment block" << std::endl;
  int level = 1;
  while (level > 0) {
    if (peek() == '\0') {
      error_reporter.report(line, "Comment block not closed");
      break;
    }

    if (peek() == '*' && peekNext() == '/') {
      advance();  // consume '*'
      --level;
    } else if (peek() == '/' && peekNext() == '*') {
      advance();  // consume '/'
      ++level;
    } else if (peek() == '\n')
      ++line;

    advance();
  }
}

char Scanner::peek() {
  if (isAtEnd()) return '\0';
  return source[current];
}

char Scanner::peekNext() {
  if (current + 1 >= source.length()) return '\0';
  return source[current + 1];
}

char Scanner::advance() { return source[current++]; }

void Scanner::addToken(TokenType type) { addToken(type, std::any(nullptr)); }

void Scanner::addToken(TokenType type, std::any literal) {
  std::string text = source.substr(start, current - start);
  tokens.push_back(Token(type, text, literal, line));
}

bool Scanner::match(char expected) {
  if (peek() != expected) return false;

  current++;
  return true;
}

}  // namespace cpplox
