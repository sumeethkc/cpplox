#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>

#include "token.h"

inline std::string readFile(const std::string &source) {
  std::ifstream in(source, std::ios::in | std::ios::binary);
  if (in) {
    std::ostringstream contents;
    contents << in.rdbuf();
    in.close();
    return contents.str();
  } else {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return "";
  }
}

inline std::map<std::string, int> getTokenMap(std::list<cpplox::Token> tokens) {
  std::map<std::string, int> token_counts;
  for (auto token : tokens) {
    std::cout << token.toString() << std::endl;
    token_counts[token.getTokenType()]++;
  }

  for (auto pair : token_counts) {
    std::cout << pair.first << ":" << pair.second << std::endl;
  }

  return token_counts;
}
#endif
