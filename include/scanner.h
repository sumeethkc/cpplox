#ifndef SCANNER_H
#define SCANNER_H
#pragma once

#include <list>
#include <string>

#include "error_reporter.h"
#include "token.h"

namespace cpplox {

class Scanner {
 public:
  Scanner(const std::string &source, ErrorReporter &error_reporter)
      : source(source), error_reporter(error_reporter) {}

  std::list<Token> scanTokens();

 private:
  const std::string &source;
  std::list<Token> tokens;
  ErrorReporter error_reporter;

  // scanner state variables
  int start = 0;
  int current = 0;
  int line = 1;

  bool isAtEnd();
  void scanToken();
  char advance();
  bool match(char expected);
  char peek();
  char peekNext();
  void skipComment();
  void skipCommentBlock();
  void createStringToken();
  bool isDigit(char c);
  bool isAlpha(char c);
  bool isAlphaNumeric(char c);
  void identifier();
  void number();

  void addToken(TokenType type);
  void addToken(TokenType type, std::any literal);
  TokenType::Value getKeywordType(const std::string &text);
};
}  // namespace cpplox
#endif  // SCANNER_H
