#include "gtest/gtest.h"
#include "token.h"

TEST(TokenTest, toStringTest) {
  cpplox::Token t(cpplox::TokenType::Value::LEFT_PAREN, "(", std::any(nullptr),
                  100);
  EXPECT_EQ("LEFT_PAREN ( 100", t.toString());
}

TEST(TokenTest, toStringTest1) {
  auto a = std::string("some literal");
  cpplox::Token t(cpplox::TokenType::Value::STRING, "some literal", a, 100);
  EXPECT_EQ("STRING some literal some literal 100", t.toString());
}
