#include <fstream>
#include <iostream>

#include "../test/test_utils/utils.h"
#include "error_reporter.h"
#include "gtest/gtest.h"
#include "scanner.h"
#include "token.h"
#include "token_type.h"

TEST(ScannerTest, scanTokensTest) {
  std::string source("int i = 10;");
  cpplox::ErrorReporter error_reporter;
  cpplox::Scanner scanner(source, error_reporter);

  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.count("EQUAL"), 1);
  EXPECT_EQ(token_counts.find("IDENTIFIER")->second, 2);
  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
  EXPECT_EQ(token_counts.count("NUMBER"), 1);
  EXPECT_EQ(token_counts.count("SEMICOLON"), 1);
}

TEST(ScannerTest, identifiersTest) {
  std::string file = "../test/scanning/identifiers.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.find("IDENTIFIER")->second, 8);
  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
}

TEST(ScannerTest, keywordsTest) {
  std::string file = "../test/scanning/keywords.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  std::list<std::string> expectedTokens{
      "AND", "CLASS",  "ELSE",  "FALSE", "FOR",  "FUN", "IF",    "NIL",
      "OR",  "RETURN", "SUPER", "THIS",  "TRUE", "VAR", "WHILE", "LOX_EOF"};

  for (auto token : expectedTokens) {
    EXPECT_EQ(token_counts.count(token), 1);
  }
}

TEST(ScannerTest, numbersTest) {
  std::string file = "../test/scanning/numbers.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.find("NUMBER")->second, 4);
}

TEST(ScannerTest, punctuatorsTest) {
  std::string file = "../test/scanning/punctuators.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  std::list<std::string> expectedTokens{
      "BANG_EQUAL",    "COMMA",      "DOT",        "EQUAL_EQUAL", "GREATER",
      "GREATER_EQUAL", "LEFT_BRACE", "LEFT_PAREN", "LESS",        "LESS_EQUAL",
      "LOX_EOF",       "MINUS",      "PLUS",       "RIGHT_BRACE", "RIGHT_PAREN",
      "SEMICOLON",     "SLASH"};

  for (auto token : expectedTokens) {
    EXPECT_EQ(token_counts.count(token), 1);
  }
}

TEST(ScannerTest, stringsTest) {
  std::string file = "../test/scanning/strings.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.find("STRING")->second, 2);
}

TEST(ScannerTest, whitespaceTest) {
  std::string file = "../test/scanning/whitespace.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.find("IDENTIFIER")->second, 4);
}
