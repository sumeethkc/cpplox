#ifndef TOKEN_TYPE_H
#define TOKEN_TYPE_H
#pragma once

#include <map>
#include <string>

namespace cpplox {
class TokenType {
 public:
  enum class Value {
    // Single-character tokens.
    LEFT_PAREN,
    RIGHT_PAREN,
    LEFT_BRACE,
    RIGHT_BRACE,
    COMMA,
    DOT,
    MINUS,
    PLUS,
    SEMICOLON,
    SLASH,
    STAR,

    // One or two characters tokens.
    BANG,
    BANG_EQUAL,
    EQUAL,
    EQUAL_EQUAL,
    GREATER,
    GREATER_EQUAL,
    LESS,
    LESS_EQUAL,

    // Literals.
    IDENTIFIER,
    STRING,
    NUMBER,

    // Keywords.
    AND,
    CLASS,
    ELSE,
    FALSE,
    FUN,
    FOR,
    IF,
    NIL,
    OR,
    PRINT,
    RETURN,
    SUPER,
    THIS,
    TRUE,
    VAR,
    WHILE,

    LOX_EOF
  };
  using TokenTypeMap = std::map<Value, std::string>;

  TokenType() {}
  TokenType(Value type) : value(type) {}
  std::string toString();

 private:
  Value value;
  static TokenTypeMap tokenTypeStrings;
};
}  // namespace cpplox

#endif
