#include "token_type.h"

#include <map>
#include <string>
namespace cpplox {

TokenType::TokenTypeMap TokenType::tokenTypeStrings = [] {
  TokenTypeMap ret{
      {TokenType::Value::LEFT_PAREN, "LEFT_PAREN"},
      {TokenType::Value::RIGHT_PAREN, "RIGHT_PAREN"},
      {TokenType::Value::LEFT_BRACE, "LEFT_BRACE"},
      {TokenType::Value::RIGHT_BRACE, "RIGHT_BRACE"},
      {TokenType::Value::COMMA, "COMMA"},
      {TokenType::Value::DOT, "DOT"},
      {TokenType::Value::MINUS, "MINUS"},
      {TokenType::Value::PLUS, "PLUS"},
      {TokenType::Value::SEMICOLON, "SEMICOLON"},
      {TokenType::Value::SLASH, "SLASH"},
      {TokenType::Value::STAR, "STAR"},
      {TokenType::Value::BANG, "BANG"},
      {TokenType::Value::BANG_EQUAL, "BANG_EQUAL"},
      {TokenType::Value::EQUAL, "EQUAL"},
      {TokenType::Value::EQUAL_EQUAL, "EQUAL_EQUAL"},
      {TokenType::Value::GREATER, "GREATER"},
      {TokenType::Value::GREATER_EQUAL, "GREATER_EQUAL"},
      {TokenType::Value::LESS, "LESS"},
      {TokenType::Value::LESS_EQUAL, "LESS_EQUAL"},
      {TokenType::Value::IDENTIFIER, "IDENTIFIER"},
      {TokenType::Value::STRING, "STRING"},
      {TokenType::Value::NUMBER, "NUMBER"},
      {TokenType::Value::AND, "AND"},
      {TokenType::Value::CLASS, "CLASS"},
      {TokenType::Value::ELSE, "ELSE"},
      {TokenType::Value::FALSE, "FALSE"},
      {TokenType::Value::FUN, "FUN"},
      {TokenType::Value::FOR, "FOR"},
      {TokenType::Value::IF, "IF"},
      {TokenType::Value::NIL, "NIL"},
      {TokenType::Value::OR, "OR"},
      {TokenType::Value::PRINT, "PRINT"},
      {TokenType::Value::RETURN, "RETURN"},
      {TokenType::Value::SUPER, "SUPER"},
      {TokenType::Value::THIS, "THIS"},
      {TokenType::Value::TRUE, "TRUE"},
      {TokenType::Value::VAR, "VAR"},
      {TokenType::Value::WHILE, "WHILE"},
      {TokenType::Value::LOX_EOF, "LOX_EOF"}};

  return ret;
}();

std::string TokenType::toString() {
  auto it = TokenType::tokenTypeStrings.find(value);
  return it == TokenType::tokenTypeStrings.end() ? "Out of range" : it->second;
}
}  // namespace cpplox
