#ifndef ERROR_REPORTER_H
#define ERROR_REPORTER_H
#pragma once

#include <string>

namespace cpplox {

enum struct ErrorStatus { OK, ERROR };
class ErrorReporter {
 public:
  void report(int line, const std::string &message);
  ErrorStatus getStatus();
 private:
  ErrorStatus status = ErrorStatus::OK;
};
}  // namespace cpplox
#endif  // ERROR_REPORTER_H
