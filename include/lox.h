#ifndef LOX_H
#define LOX_H
#pragma once

#include <string>

#include "error_reporter.h"

namespace cpplox {

class Lox {
 public:
  Lox(ErrorReporter &error_reporter) : error_reporter(error_reporter) {}
  virtual int runFile(char const *path);
  virtual int runPrompt();
  void error(int line, const std::string &message);

 private:
  virtual int run(const std::string &source);

  ErrorReporter error_reporter;
};
}  // namespace cpplox
#endif  // LOX_H
