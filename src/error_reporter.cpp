#include "error_reporter.h"

#include <iostream>
#include <string>
namespace cpplox {

void ErrorReporter::report(int line, const std::string &message) {
  status = ErrorStatus::ERROR;
  std::cerr << "[Line: " << line << "] Error: " << message << std::endl;
}

ErrorStatus ErrorReporter::getStatus() { return status; }
}  // namespace cpplox
