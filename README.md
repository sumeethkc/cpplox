# README #

Implementing lox language in C++ following the crafting interpreters book (http://www.craftinginterpreters.com/)

### What is this repository for? ###

Lox is a learning exercise in implementing a high-level language with a C-like syntax. It supports Dynamic typing 
and automatic memory management. 

### How do I get set up? ###

* Summary of set up
* Configure and build:
	* mkdir build
	* cmake ../
	* make
* Dependencies:
	* Build tool [cmake](https://cmake.org/cmake/help/v3.10/release/3.10.html)
	* Testing Framework: [GoogleTest](https://github.com/google/googletest) release v1.10.0
* How to run tests:
	* cd build
	* ./cpplox_tests
