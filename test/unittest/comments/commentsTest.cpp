#include <fstream>
#include <iostream>

#include "../test/test_utils/utils.h"
#include "error_reporter.h"
#include "gtest/gtest.h"
#include "scanner.h"
#include "token.h"
#include "token_type.h"

TEST(CommentsTest, lineAtEOFTest) {
  std::string file = "../test/comments/line_at_eof.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.count("PRINT"), 1);
  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
}

TEST(CommentsTest, onlyLineCommentTest) {
  std::string file = "../test/comments/only_line_comment.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
}

TEST(CommentsTest, unicodeInCommentTest) {
  std::string file = "../test/comments/unicode.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.count("PRINT"), 1);
  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
}

TEST(CommentsTest, onlyLineCommentAndLineTest) {
  std::string file = "../test/comments/only_line_comment_and_line.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
}

TEST(CommentsTest, blockCommentsTest) {
  std::string file = "../test/comments/block_comment.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.count("PRINT"), 1);
  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
}

TEST(CommentsTest, nestedBlockCommentsTest) {
  std::string file = "../test/comments/nested_block_comment.lox";
  cpplox::ErrorReporter error_reporter;
  std::string source = readFile(file);

  cpplox::Scanner scanner(source, error_reporter);
  std::list<cpplox::Token> tokens = scanner.scanTokens();
  std::map<std::string, int> token_counts = getTokenMap(tokens);

  EXPECT_EQ(token_counts.count("PRINT"), 1);
  EXPECT_EQ(token_counts.count("LOX_EOF"), 1);
}
