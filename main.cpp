#include <iostream>

#include "lox.h"

int main(int argc, char const *argv[]) {
  if (argc > 2) {
    std::cout << "Usage: cpplox [script]" << std::endl;
    std::exit(64);
  } else if (argc == 2) {
    cpplox::ErrorReporter error_reporter;
    cpplox::Lox l(error_reporter);
    return l.runFile(argv[1]);
  } else {
    cpplox::ErrorReporter error_reporter;
    cpplox::Lox l(error_reporter);
    return l.runPrompt();
  }
  return 0;
}
