#include "gtest/gtest.h"
#include "token_type.h"

class TokenTypeToStringTests : public ::testing::TestWithParam<
                                   std::tuple<std::string, cpplox::TokenType>> {
 protected:
  cpplox::TokenType t;
};

TEST_P(TokenTypeToStringTests, toStringTest) {
  std::string expected = std::get<0>(GetParam());
  cpplox::TokenType t = std::get<1>(GetParam());
  // cpplox::TokenType::Value::LEFT_PAREN;dd
  EXPECT_EQ(expected, t.toString());
}

INSTANTIATE_TEST_SUITE_P(
    ToStringTests, TokenTypeToStringTests,
    ::testing::Values(
        std::make_tuple("LEFT_PAREN", cpplox::TokenType::Value::LEFT_PAREN),
        std::make_tuple("RIGHT_PAREN", cpplox::TokenType::Value::RIGHT_PAREN),
        std::make_tuple("LEFT_BRACE", cpplox::TokenType::Value::LEFT_BRACE),
        std::make_tuple("COMMA", cpplox::TokenType::Value::COMMA),
        std::make_tuple("DOT", cpplox::TokenType::Value::DOT),
        std::make_tuple("MINUS", cpplox::TokenType::Value::MINUS),
        std::make_tuple("PLUS", cpplox::TokenType::Value::PLUS),
        std::make_tuple("SEMICOLON", cpplox::TokenType::Value::SEMICOLON),
        std::make_tuple("SLASH", cpplox::TokenType::Value::SLASH),
        std::make_tuple("STAR", cpplox::TokenType::Value::STAR),
        std::make_tuple("BANG", cpplox::TokenType::Value::BANG),
        std::make_tuple("BANG_EQUAL", cpplox::TokenType::Value::BANG_EQUAL),
        std::make_tuple("EQUAL", cpplox::TokenType::Value::EQUAL),
        std::make_tuple("EQUAL_EQUAL", cpplox::TokenType::Value::EQUAL_EQUAL),
        std::make_tuple("GREATER", cpplox::TokenType::Value::GREATER),
        std::make_tuple("GREATER_EQUAL",
                        cpplox::TokenType::Value::GREATER_EQUAL),
        std::make_tuple("LESS", cpplox::TokenType::Value::LESS),
        std::make_tuple("LESS_EQUAL", cpplox::TokenType::Value::LESS_EQUAL),
        std::make_tuple("IDENTIFIER", cpplox::TokenType::Value::IDENTIFIER),
        std::make_tuple("STRING", cpplox::TokenType::Value::STRING),
        std::make_tuple("NUMBER", cpplox::TokenType::Value::NUMBER),
        std::make_tuple("AND", cpplox::TokenType::Value::AND),
        std::make_tuple("CLASS", cpplox::TokenType::Value::CLASS),
        std::make_tuple("ELSE", cpplox::TokenType::Value::ELSE),
        std::make_tuple("FALSE", cpplox::TokenType::Value::FALSE),
        std::make_tuple("FUN", cpplox::TokenType::Value::FUN),
        std::make_tuple("FOR", cpplox::TokenType::Value::FOR),
        std::make_tuple("IF", cpplox::TokenType::Value::IF),
        std::make_tuple("NIL", cpplox::TokenType::Value::NIL),
        std::make_tuple("OR", cpplox::TokenType::Value::OR),
        std::make_tuple("PRINT", cpplox::TokenType::Value::PRINT),
        std::make_tuple("RETURN", cpplox::TokenType::Value::RETURN),
        std::make_tuple("SUPER", cpplox::TokenType::Value::SUPER),
        std::make_tuple("THIS", cpplox::TokenType::Value::THIS),
        std::make_tuple("TRUE", cpplox::TokenType::Value::TRUE),
        std::make_tuple("VAR", cpplox::TokenType::Value::VAR),
        std::make_tuple("WHILE", cpplox::TokenType::Value::WHILE),
        std::make_tuple("LOX_EOF", cpplox::TokenType::Value::LOX_EOF)));
