#include "lox.h"

#include <fstream>
#include <iostream>
#include <list>
#include <sstream>
#include <string>

#include "scanner.h"

namespace cpplox {

int Lox::runFile(char const *path) {
  std::ifstream in(path, std::ios::in | std::ios::binary);
  if (in) {
    std::ostringstream contents;
    contents << in.rdbuf();
    in.close();
    int status = run(contents.str());
    if (error_reporter.getStatus() == ErrorStatus::ERROR) status = 65;
    return status;
  } else {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return errno;
  }
}

int Lox::runPrompt() {
  std::string line;
  int status = 0;
  do {
    std::cout << "> ";
    std::getline(std::cin, line);
    status = run(line);
  } while (!line.empty());

  return status;
}

int Lox::run(const std::string &source) {
  Scanner scanner(source, error_reporter);
  std::list<Token> tokens = scanner.scanTokens();

  std::cout << "Line TokenType lexeme literal" << std::endl;
  for (auto token : tokens) {
    std::cout << token.toString() << std::endl;
  }
  return 0;
}

void Lox::error(int line, const std::string &message) {
  error_reporter.report(line, message);
}
}  // namespace cpplox
