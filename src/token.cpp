#include "token.h"

#include <string>

namespace cpplox {
Token::Token(TokenType type, const std::string &lexeme, std::any literal,
             int line)
    : type(type), lexeme(lexeme), literal(literal), line(line) {}

std::string Token::toString() {
  std::string ret = type.toString() + " " + lexeme;
  if (literal.type() == typeid(std::string)) {
    ret += " ";
    ret += std::any_cast<std::string>(literal);
  }

  ret += " ";
  ret += std::to_string(line);
  return ret;
}

std::string Token::getTokenType() { return type.toString(); }
}  // namespace cpplox
